/*
 ============================================================================
 Name        : readMAC.c
 Author      : Bryan Wilcutt
 Version     :
 Copyright   : Copyright (c) 2016 Lynxspring, Inc. - All Rights Reserved
 Description : Hello World in C, Ansi-style
 ============================================================================
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>

#define CTL_MOD_START 0x44e10000
#define CTL_MOD_END   0x44e11fff
#define CTL_MOD_SIZE (CTL_MOD_END - CTL_MOD_START)

#define MAC_OFFSET 0x630

int main(void)
{
	int fd;
	int i;
	unsigned char *p = NULL;
	unsigned char mac[6];

	fd = open("/dev/mem", O_RDWR | O_SYNC);

	p = (unsigned char *) mmap(0, CTL_MOD_SIZE, PROT_READ | PROT_WRITE, MAP_SHARED, fd, CTL_MOD_START);

	mac[0] = p[MAC_OFFSET + 4];
	mac[1] = p[MAC_OFFSET + 5];
	mac[2] = p[MAC_OFFSET + 6];
	mac[3] = p[MAC_OFFSET + 7];
	mac[4] = p[MAC_OFFSET + 0];
	mac[5] = p[MAC_OFFSET + 1];

	for (i = 0; i < 6; i++)
	{
		printf("Byte %d: 0x%0x\n", i, mac[i]);
	}

	return EXIT_SUCCESS;
}
